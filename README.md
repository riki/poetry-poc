# Poetry POC
ref. https://github.com/python-poetry/poetry

## Requirements
Create a Python virtual environment with your tool of choice
and then install `poetry`:
```bash
pip install poetry
```

## Configuration
The whole project management is handled by a
single file: `pyproject.toml` (that is also the standard introduced by PEP518), no
need for having and managing a whole bunch of configuration file, i.e.:
```bash
Pipfile
requirements.txt
setup.py
setup.cfg
MANIFEST.in
```

Make sure this file contains all the information needed by your project. For a
`pyproject.toml` example, see the last section.

## How it works
`poetry` reads `pyproject.toml` and manages the environment and python package based
on that information.

It supports a wide variety of commands, some of the most common are:
```bash
add                        Adds a new dependency to pyproject.toml.
build                      Builds a package, as a tarball and a wheel by default.
init                       Creates a basic pyproject.toml file in the current directory.
install                    Installs the project dependencies.
lock                       Locks the project dependencies.
new                        Creates a new Python project at <path>.
publish                    Publishes a package to a remote repository.
remove                     Removes a package from the project dependencies.
run                        Runs a command in the appropriate environment.
shell                      Spawns a shell within the virtual environment.
show                       Shows information about packages.
update                     Update the dependencies as according to pyproject.toml.

env
  env info                   Displays information about the current environment.
  env list                   Lists all virtualenvs associated with the current project.

self
  self add                   Add additional packages to Poetry's runtime environment.
  self update                Updates Poetry to the latest version.

source
  source add                 Add source configuration for project.
  source remove              Remove source configured for the project.
```

For the full list of avilable commands, run `poetry list`.

## Usage
### Install dependencies
The `install` command reads the `pyproject.toml` file from the current
project, resolves the dependencies, and installs them.

If there is a `poetry.lock` file in the current directory, it will use
the exact versions from there instead of resolving them. This ensures
that everyone using the library will get the same versions of the
dependencies.

#### Install with dev dependencies:
```bash
poetry install
```

#### Install without dev dependencies:
You can specify to the command that you do not want the development
dependencies installed by passing the `--without dev` option.
```bash
poetry install --without dev
```

## Sources
If some of the dependencies are not available in the
default PyPi repository, we need to add additional sources
where to look for the dependencies.
```bash
poetry source add fury https://pypi.fury.io/rentspree/
poetry config http-basic.fury ${GEMFURY_TOKEN} NOPASS
```

### Update dependencies
In order to get the latest versions of the dependencies and to update
the `poetry.lock` file, you should use the `update` command.
```bash
poetry update
```

This will resolve all dependencies of the project and write the exact
versions into `poetry.lock`.

Alternatively, if you just want to update a few packages and not all,
you can list them as such:
```bash
poetry update requests toml
```

### Add a dependency
The `add` command adds required packages to your `pyproject.toml` and installs them.

```bash
poetry add pendulum@^2.0.5
```

### Run a command
The `run` command executes the provided command inside the
virtual environment.
```bash
poetry run python -m main
```

### Build a python package
The `build` command builds the python package.
```bash
poetry build
```

### Publish a python package
The `publish` command uploads the python package to a
repository.
```bash
poetry publish
```

For this project gitlab PyPi repository:
```bash
poetry config repositories.gitlab "https://gitlab.com/api/v4/projects/riki%2Fpoetry-poc/packages/pypi"
poetry config http-basic.gitlab gitlab-token ${GITLAB_TOKEN}
poetry publish --repository gitlab
```

### Plugins
A cool feature of `poetry` is its ability of support
integrate with external plugins.

For example, a useful plugin is `poetry-dynamic-versioning`.
```bash
poetry self add "poetry-dynamic-versioning[plugin]"
poetry dynamic-versioning enable
```

This can be used to automatically update the package
version in-place when running relevant commands with
`poetry`.

## Example
`pyproject.toml`:
```toml
[tool.poetry]
name = "my-awesome-project"
version = "0.1.0"
description = "This is a sample project."
authors = ["Riki <riccardo@rentspree.com>"]

[tool.poetry.dependencies]
python = "~3.10"
requests = "^2.28.0"
fastapi = "~0.95"
flask = ">=1.1, <2"
django = "4.1.9"
tqdm = "4.*"

[tool.poetry.dev-dependencies]
black = "*"
pytest = "*"
pytest-cov = "*"
pytest-html = "*"

# To add a private PyPI registry, uncomment the lines below and provide proper
# name and url.
# [[tool.poetry.source]]
# name = "fury"
# url = "https://pypi.fury.io/my-namespace/"
# secondary = true

[tool.black]
line-length = 120

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
```
